package com.futegolo.futnews;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import com.futegolo.futnews.SharedPrefManager;

/**
 * Created by Pai on 05/02/2017.
 */

public class simplenews extends AppCompatActivity {
    public String ImageID;
    public String ImageName;
    public String ImageFullPath;
    public String ImageContent;
    public String ImageUP;
    private ImageButton LikeButton;
    public int controla = 1;
    String user_id, noticia_id, ratings ;

    int rating;

        SharedPrefManager abc = new SharedPrefManager(this);

    public TextView title, news, like;
    ImageView networkImageView;
    private FloatingActionButton fab;
    View v;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simplenews);


        Intent intent = getIntent();
        ImageID = String.valueOf(intent.getStringExtra("ImageID"));
        ImageName = String.valueOf(intent.getStringExtra("ImageName"));
        ImageFullPath = String.valueOf(intent.getStringExtra("ImagePathFull"));
        ImageContent = String.valueOf(intent.getStringExtra("ImageContent"));
        ImageContent = String.valueOf(intent.getStringExtra("ImageContent"));
        ImageUP = String.valueOf(intent.getStringExtra("ImageUP"));

        user_id = String.valueOf(abc.getId());
        Log.d("VALUE OF user_ID",user_id);
        noticia_id = String.valueOf(intent.getStringExtra("ImageID"));
        Log.d("VALUE OF noticia_ID",noticia_id);
        ratings = ImageUP;
        Log.d("VALUE OF ratings_ID",ratings);



        title = (TextView) findViewById(R.id.textView);
        title.setText(ImageName);

        news = (TextView) findViewById(R.id.textView2);
        news.setText(ImageContent);

        like = (TextView) findViewById(R.id.textView3);
        like.setText(ImageUP);



        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, login.class));
        }


        LikeButton = (ImageButton)findViewById(R.id.imageButton);


        networkImageView = (ImageView) findViewById(R.id.imageView) ;


        Picasso.with(this).load(ImageFullPath).into(networkImageView);

        fab = (FloatingActionButton) findViewById(R.id.floatingActionButton2);



        fab.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {

                                       Intent newActivity  = new Intent(simplenews.this, FbCommentsActivity.class);

                                       newActivity.putExtra("ImageID", ImageID);

                                      newActivity.putExtra("ImageName", ImageName);

                                       newActivity.putExtra("ImagePathFull", ImageFullPath);

                                       newActivity.putExtra("ImageContent", ImageContent);

                                       //intent.putExtra("url", "http://www.androidhive.info/2016/06/android-firebase-integrate-analytics/");

                                      startActivity(newActivity);



                                       finish();
                                   }
                               });

        LikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    SendDataToServer(user_id, noticia_id);

                if(controla == 1)
                {


                    Toast.makeText(simplenews.this, "You have already vote in this News", Toast.LENGTH_LONG).show();


                }

                    /*Intent intent = new Intent(SignupActivity.this, login.class);
                    startActivity(intent);
                    SignupActivity.this.finish();*/




            }
        });


        // Show Image Full


        // rating


        // Button Home
        Button home = (Button) findViewById(R.id.button4);
        // Perform action on click
        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent newActivity = new Intent(simplenews.this, MainActivity.class);
                startActivity(newActivity);
            }
        });


    }


    public void SendDataToServer(final String user_id, final String noticia_id){
        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {

                String QuickName = user_id ;
                String QuickEmail = noticia_id ;


                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("user_id", QuickName));
                nameValuePairs.add(new BasicNameValuePair("noticia_id", QuickEmail));

                try {
                    HttpClient httpClient = new DefaultHttpClient();

                    HttpPost httpPost = new HttpPost(Configs.Likes);

                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    HttpResponse response = httpClient.execute(httpPost);

                    HttpEntity entity = response.getEntity();


                } catch (ClientProtocolException e) {

                } catch (IOException e) {

                }
                return "Data Submit Successfully";
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                Toast.makeText(simplenews.this, "Data Submit Successfully", Toast.LENGTH_LONG).show();
                controla = 1;
                Intent newActivity = new Intent(simplenews.this,last_internacional.class);
                startActivity(newActivity);

            }
        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(user_id, noticia_id);
    }





}
