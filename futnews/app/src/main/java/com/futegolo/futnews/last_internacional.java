package com.futegolo.futnews;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.futegolo.futnews.Recycler.lovesnews;

public class last_internacional extends AppCompatActivity {

    List<News> GetDataAdapter1;

    RecyclerView recyclerView;



    RecyclerView.LayoutManager recyclerViewlayoutManager;

    RecyclerView.Adapter recyclerViewadapter;

    // String GET_JSON_DATA_HTTP_URL = "http://androidblog.esy.es/ImageJsonData.php";
    //String JSON_IMAGE_TITLE_NAME = "image_title";
    //String JSON_IMAGE_URL = "image_url";

    JsonArrayRequest jsonArrayRequest ;

    RequestQueue requestQueue ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.last_internacional);

        

        GetDataAdapter1 = new ArrayList<>();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview1);

        recyclerView.setHasFixedSize(true);

        recyclerViewlayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(recyclerViewlayoutManager);




        JSON_DATA_WEB_CALL();

      /*  bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {

                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {




                            case R.id.buscarItem:
                                // Pesquisa....

                                Intent intents = new Intent(last_internacional.this, Search.class);
                                startActivity(intents);
                                finish();

                                break;

                            case R.id.favoritosItem:
                                // Mais rating....

                                break;

                            case R.id.videos:
                                // Perfil....

                                break;

                            case R.id.photos:
                                // Perfil....
                                Intent intentss = new Intent(last_internacional.this, lastnews.class);
                                startActivity(intentss);
                                finish();

                                break;
                        }
                        return false;
                    }
                });*/


    }

    public void JSON_DATA_WEB_CALL(){

        jsonArrayRequest = new JsonArrayRequest(Configs.Nacional,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        JSON_PARSE_DATA_AFTER_WEBCALL(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.getCache().clear();


        requestQueue.add(jsonArrayRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menus, menu);
        return true;
    }


   public boolean onOptionsItemSelected(MenuItem item) {

       switch (item.getItemId()) {




           case R.id.buscarItem:
               // Pesquisa....

               Intent intents = new Intent(last_internacional.this, MainActivity.class);
               startActivity(intents);
               finish();

               break;

           case R.id.favoritosItem:
               // Mais rating....

               Intent intentssf = new Intent(last_internacional.this, lovesnews.class);
               startActivity(intentssf);
               finish();



               break;

           case R.id.videos:
               // Perfil....

               Intent intentados = new Intent(last_internacional.this, videos.class);
               startActivity(intentados);
               finish();

               break;

           case R.id.photos:
               // Perfil....
               Intent intentss = new Intent(last_internacional.this, lastnews.class);
               startActivity(intentss);
               finish();

               break;

           default:

               super.onOptionsItemSelected(item);
       }

        return true;
    }

    public void JSON_PARSE_DATA_AFTER_WEBCALL(JSONArray array){

        for(int i = 0; i<array.length(); i++) {

            News GetDataAdapter2 = new News();

            JSONObject json = null;
            try {

                json = array.getJSONObject(i);

                GetDataAdapter2.setName(json.getString(Configs.TAG_NAME));
                GetDataAdapter2.setId(json.getString(Configs.TAG_IMAGE_ID));
                GetDataAdapter2.setPublisher(json.getString(Configs.TAG_PUBLISHER));
                GetDataAdapter2.setImageUrl(json.getString(Configs.TAG_IMAGE_URL));


                if(json.has(Configs.UP)) {
                    GetDataAdapter2.setUps(json.optString("up"));
                    Log.i("UP", String.valueOf(json.getInt("up")));
                }




            } catch (JSONException e) {

                e.printStackTrace();
            }
            GetDataAdapter1.add(GetDataAdapter2);
        }


        recyclerViewadapter = new RecyclerAdapter(GetDataAdapter1, this);

        recyclerView.setAdapter(recyclerViewadapter);
      //  recyclerViewadapter.notifyDataSetChanged();


    }
}
