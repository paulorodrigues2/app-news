package com.futegolo.futnews;

/**
 * Created by Pai on 06/02/2017.
 */

public class user {

   private String iduser;
    private String username;
    private String email;

    public String getIdusername()
    {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
