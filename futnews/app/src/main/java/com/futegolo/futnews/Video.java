package com.futegolo.futnews;

/**
 * Created by Pai on 15/02/2017.
 */

public class Video {

    //Data Variables
    private String videoUrl;
    private String name;
    private String like_video;
    private String id;


    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getLike_video() {
        return like_video;
    }

    public void setLike_video(String like_video) {
        this.like_video = like_video;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
