package com.futegolo.futnews;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

/**
 * Created by JUNED on 6/16/2016.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    Context context;

    List<News> getDataAdapter;

    ImageLoader imageLoader1;


    public RecyclerAdapter(List<News> getDataAdapter, Context context){

        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;
    }






    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_news, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, int position) {

        News getDataAdapter1 =  getDataAdapter.get(position);

        imageLoader1 = ServerImageParseAdapter.getInstance(context).getImageLoader();

        imageLoader1.get(getDataAdapter1.getImageUrl(),
                ImageLoader.getImageListener(
                        Viewholder.networkImageView,//Server Image
                        R.mipmap.ic_launcher,//Before loading server image the default showing image.
                        android.R.drawable.ic_dialog_alert //Error image if requested image dose not found on server.
                )
        );

        Viewholder.networkImageView.setImageUrl(getDataAdapter1.getImageUrl(), imageLoader1);

        Viewholder.ImageTitleNameView.setText(getDataAdapter1.getName());
        Viewholder.likes.setText(getDataAdapter1.getUps());

       // text for content of news  -> Viewholder.NewsTextNameView.setText(getDataAdapter1.geText());
    }

    @Override
    public int getItemCount() {

        return getDataAdapter.size();
    }




  /*  receiptlist.clear();
    receiptlist.addAll(newlist);
    this.notifyDataSetChanged();*/

   public void notify(List<News> list) {
        if (getDataAdapter != null) {
            getDataAdapter.clear();
            getDataAdapter.addAll(list);

        } else {
            getDataAdapter = list;
        }
        notifyDataSetChanged();
    }

    /*public void swapDataSet(list<News> newData){

        this.news = newData;

        //now, tell the adapter about the update
        notifyDataSetChanged();

    }*/

    class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{

        public TextView ImageTitleNameView, likes;
       // public TextView NewsTextNameView;
        public NetworkImageView networkImageView ;

        public Button buttonid;

        public ViewHolder(View itemView) {

            super(itemView);

            ImageTitleNameView = (TextView) itemView.findViewById(R.id.textView_item) ;
           // NewsTextNameView = (TextView) itemView.findViewById(R.id.test) ;

            likes = (TextView) itemView.findViewById(R.id.textView4);

            networkImageView = (NetworkImageView) itemView.findViewById(R.id.VollyNetworkImageView1) ;

            buttonid= (Button)  itemView.findViewById(R.id.button6) ;

            buttonid.setOnClickListener(this);

        }



        public void onClick(View v) {

            int position  =   getAdapterPosition();


            switch (v.getId()){
                case R.id.button6:

                    String strImageID = getDataAdapter.get(position).getId().toString();
                    String strImageName = getDataAdapter.get(position).getName().toString();
                    String strImage = getDataAdapter.get(position).getImageUrl().toString();
                    String strText = getDataAdapter.get(position).geText().toString();
                    String up = getDataAdapter.get(position).getUps().toString();




                   // Log.w("","Selected"+up);
                    Log.w("","Selected"+strImage);
                    Log.w("","Selected"+strText);
                    // Log.w("", "Selected"+position);
                    //Toast.makeText(context, String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();






                    Intent newActivity = new Intent(v.getContext(), simplenews.class);

                    newActivity.putExtra("ImageID", strImageID);

                    newActivity.putExtra("ImageName", strImageName);

                    newActivity.putExtra("ImagePathFull", strImage);

                    newActivity.putExtra("ImageContent", strText);

                    newActivity.putExtra("ImageUP", up);





                    context.startActivity(newActivity);


                    break;
            }



        }
    }
}