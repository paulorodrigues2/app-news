package com.futegolo.futnews;

/**
 * Created by Pai on 31/01/2017.
 */

public class Configs {

    public final static String URL = "http://www.futegolo.com/";
    public final static String Nacional = URL + "getnacional.php";
    public final static String Internacional = URL + "getinternational.php";
    public final static String Mercado = URL + "getmercado.php";
    public final static String imagens = URL + "getimage.php";
    public final static String Login = URL + "loginuser.php";
    public static final String UPLOAD_URL = URL+"upload.php";
    public static final String IMAGES_URL = URL +"getImages.php";
    public static final String Search = URL + "searches.php";
    public static final String Register = URL + "insert.php";
    public static final String Likes = URL + "insertlikes.php";
    public static final String Likes_show = URL + "getlikes.php";
    public static final String Videos = URL + "getvideos.php";
    public static final String Fixture = URL + "getgame.php";
    public static final String Comments_Fixture = URL + "getcomments.php";
    public static final String Comments_insert = URL + "insertcomments.php";


    //JSON TAGS NEWS
    public static final String TAG_IMAGE_URL = "image_url";
    public static final String TAG_NAME = "title";
    public static final String TAG_PUBLISHER = "text";
    public static  final String TAG_IMAGE_NAME = "image_title";
    public static final String TAG_IMAGE_ID = "id";
    public static final String UP = "up";
    //

    //JSON TAGS VIDEOS


    public static final String TAG_VIDEO_ID = "id";
    public static final String TAG_VIDEO_CONTENT = "content";
    public static final String TAG_VIDEO_URL = "url";





}
