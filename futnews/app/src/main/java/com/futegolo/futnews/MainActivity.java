package com.futegolo.futnews;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button;
    private BottomNavigationView bottomNavigationView;
    //private FloatingActionButton fab;
    private ImageView namefirst, namesecond;
    private EditText values;
    private ProgressDialog progressDialog;
    public String home, away, id_score;
    Context contex;

    public static String user_id, texto;

    JsonArrayRequest jsonArrayRequest;

    RequestQueue requestQueue;
    List<GetDataAdapter> GetDataAdapter1;

    RecyclerView recyclerView;

    RecyclerView.LayoutManager recyclerViewlayoutManager;

    RecyclerView.Adapter recyclerViewadapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_main);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, login.class));
        }

        SharedPrefManager abc = new SharedPrefManager(this);
        namefirst = (ImageView) findViewById(R.id.imageView3);
        namesecond = (ImageView) findViewById(R.id.imageView4);
        values = (EditText) findViewById(R.id.editText3);

        button = (Button) findViewById(R.id.button11);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");

        values.getText().toString();
        button.setOnClickListener(this);

        texto = String.valueOf(values);

        user_id = String.valueOf(abc.getId());

        userLogin();

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);


        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {

                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.camaraItem:
                                // Todas as notícias...

                                Intent intent = new Intent(MainActivity.this, last_internacional.class);
                                startActivity(intent);
                                finish();


                                break;

                            case R.id.buscarItem:
                                // Pesquisa....

                                Intent intents = new Intent(MainActivity.this, Search.class);
                                startActivity(intents);
                                finish();

                                break;


                            case R.id.perfilItem:
                                // Perfil....

                                break;
                        }
                        return false;
                    }
                });
    }




    // Select Best GAME //

    private void userLogin() {


        final String email = "Name";


        progressDialog.show();

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Configs.Fixture,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            //JSONObject obj = new JSONObject(response);
                            // if(!obj.getBoolean("error")){//
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject obj = jsonArray.getJSONObject(0);


                            id_score = obj.getString("id");
                            home = obj.getString("home");
                            away = obj.getString("away");

                            Log.i("Value of fixture", id_score);
                            Log.i("Value of Home", home);
                            Log.i("Value of Away", away);


                            Picasso.with(MainActivity.this)
                                    .load(home)
                                    .into(namefirst);

                            Picasso.with(MainActivity.this)
                                    .load(away)
                                    .into(namesecond);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        Toast.makeText(
                                getApplicationContext(),
                                error.getMessage(),
                                Toast.LENGTH_LONG
                        ).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                return params;
            }

        };

        RequestHandler.getInstance(this).getRequestQueue().getCache().clear();
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }



    public void SendDataToServer(final String id_score, final String user_id, final String texto) {
        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {

                final String QuickGame = id_score;
                final String QuickUser = user_id;
                final String QuickText = texto;
                Log.i("Value of Game_id", QuickGame);

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("Game_id", QuickGame));
                nameValuePairs.add(new BasicNameValuePair("user_id", QuickUser));
                nameValuePairs.add(new BasicNameValuePair("text", QuickText));

                try {
                    HttpClient httpClient = new DefaultHttpClient();

                    HttpPost httpPost = new HttpPost(Configs.Comments_insert);

                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    HttpResponse response = httpClient.execute(httpPost);

                    HttpEntity entity = response.getEntity();


                } catch (ClientProtocolException e) {

                } catch (IOException e) {

                }
                return "Data Submit Successfully";
            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                Toast.makeText(MainActivity.this, "Data Submit Successfully", Toast.LENGTH_LONG).show();

                Intent newActivity = new Intent(MainActivity.this, MainActivity.class);
                startActivity(newActivity);

            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(id_score, user_id, texto);
        }



    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.button11:

                SendDataToServer(id_score, user_id, texto);


                break;
        }
    }



        }
