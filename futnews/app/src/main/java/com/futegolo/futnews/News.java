package com.futegolo.futnews;


/**
 * Created by Belal on 12/5/2015.
 */

public class News {

    //Data Variables
    private String imageUrl;
    private String name;
    private String text;
    private String id;
    private String up;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUps() {
        return up;
    }

    public void setUps(String up) {
        this.up = up;
    }

    //Getters and Setters
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    public String geText() {
        return text;
    }

    public void setPublisher(String publisher) {
        this.text = publisher;
    }

   public String toString(){

        return "NEWS : " + name + "\nContent : " + text + "\nimage : " + imageUrl;
    }

}